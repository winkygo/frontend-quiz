import { Person } from './person';

export default function creatPerson(response) {
  const { name, age, description, educations } = response;
  return new Person(name, age, description, educations);
}
