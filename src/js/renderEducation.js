import $ from 'jquery';
import createPerson from './createPerson';

export default function renderEducation(response) {
  const array = createPerson(response).education;
  array.forEach(item => {
    $('ul').append(
      `<li><h4 class="year">${item.year}</h4><div class="experience"><h4 class="boldTitle">${item.title}</h4><p>${item.description}</p></div></li>`
    );
  });
}
